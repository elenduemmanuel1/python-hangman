hints = [
    "This person is African-American",
    "This person has made a significant contribution to Black man's fight for equality",
    "This person has killer punchlines",
    "This person was once the leader of the free world"
]

secret_person = ["obama", hints]


def run_game():
    guess_count = 0
    guess_limit = 4
    guess_hint = f'{len(secret_person[0])} letter word'
    can_guess = True
    answer = secret_person[0]
    while guess_count < guess_limit and can_guess:
        print(hints[guess_count] + "\nYour hint: " + guess_hint)
        guess_hint = ""
        user_answer = input("Who do you think this person is? :")
        if user_answer.lower() == answer:
            can_guess = False
        else:
            for letter in answer:
                if answer.find(letter) == user_answer.find(letter.lower()):
                    guess_hint = guess_hint + letter
                else:
                    guess_hint = guess_hint + "-"
        guess_count += 1
    if not can_guess:
        print("Congrats, you win!")
    else:
        print("You are out of guesses. Wanna go again? ")


run_game()
