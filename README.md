# Simple Word Guessing Game

## Requirements
This project requires ```Python3``` to run. 

## Versions
The project is in multiple parts, you will get the most recent version of the project by doing a  ```git clone ```.

### V1
The simplest version of the app. This version lets you guess the word in 4 tries giving you a hint before each try

### V2
This version takes hinting further by telling you if a letter or letters in the word you guessed are in the correct position. 

## Running

Clone the repository

```bash
git clone https://gitlab.com/elenduemmanuel1/python-hangman.git
```

Enter the python-word-game folder

```bash
cd python-hangman
```
To start the game, run
```bash
python main.py
```

## Usage



## License
[MIT](https://choosealicense.com/licenses/mit/)